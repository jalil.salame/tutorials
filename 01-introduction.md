# Introduction

## Foreword

Over the years, I've learned, written, loved and hated a lot of programming languages. If you name a
language people have heard of, there's a good chance I've written enough of it to feel like I can
justify an opinion. The same is also true for a number of programming languages most programmers
*haven't* heard of.

Of these more niche languages, Idris is by far my favourite. Using it feels like a little taste of
the future, where software works like it's supposed to, whatever happens. I'd like you to share in
that future too, but learning idris is not easy and a lot of the materials require a certain amount
of background to understand.

This book is an attempt to make getting started with Idris more productive and fun, to gently
introduce you to the advanced concepts that underpin this amazing language in a way that is more
intuitive for a practical programmer. In time, I hope you'll grow to love Idris as much as me!

## What is idris? Where are its roots?

First and foremost, Idris is a *strongly-typed programming language*, similar to Haskell or other
strongly-typed FP languages. It has Applicatives and Monads and all that stuff. At a basic level, if
you know Haskell or Purescript or something of that ilk, idris isn't so fundamentally different to
use in practice most of the time.

Secondly, idris is a *theorem proving language*. Superficially, theorem provers often look very much
like programming languages, but where programming languages are good at *doing stuff*, theorem
provers are good at *checking correctness*. Idris mashes these two worlds together, enabling you to
*check the correctness of code*. The way it does this is through the power of dependent types.

In something like java or rust we have *generic* types, `Vec<T>` or such.  In haskell, we have
*higher kinded* types, such as `Monad m` where `m` is a parameterised type like `Vec<T>`, but
independent of a particular `T`, just the `Vec` part. To turn it into a type, we still have to
choose the `T`. We can thus see higher-kinded types as being like `type-level functions` (and we use
the same syntax when applying them.

Idris goes further, allowing *dependent* types. In dependent types, we are allowed to put *values*
into types. A trivial example is we could put the length of a vector in its type and it would be
forced to be that length by the compiler. In idris syntax, `Vect 3 Nat` is a vector with 3 items,
which are `Nat`s (natural numbers).

If that blows your mind slightly, it probably should - it blew mine at first! But this is just
scratching the surface - Idris has lots of incredible (and *fun* and *shiny!*) superpowers that
really make you reevaluate what programming languages are capable of being.

The secret to all of this is *Type Theory*.

### What is type theory?


### A little history

* 1874: Georg Cantor formalises [Set theory](https://en.wikipedia.org/wiki/Set_theory) in a paper
* 1901: Bertrand Russell publishes his [paradox](https://en.wikipedia.org/wiki/Russell%27s_paradox),
  demonstrating that set theory is not consistent.
* 1932/3: Alonzo Church invents [Lambda Calculus (LC)](https://en.wikipedia.org/wiki/Lambda_calculus)
* 1935: Stephen Kleene and John Barkley Rosser Sr (students of Alonzo Church) show lambda calculus
  inconsistent with the [Kleene-Rosser paradox](https://en.wikipedia.org/wiki/Kleene%E2%80%93Rosser_paradox)
* 1936: Church publishes the untyped lambda calculus (UTC, the computationally relevant consistent subset)
* 1940: Church publishes the simply typed lambda calculus (STLC, weaker than original LC but consistent)
* 1972: Per Martin-L�f publishes [Intuitionistic Type Theory (ITT)](https://en.wikipedia.org/wiki/Intuitionistic_type_theory)
* 1972: [System F](https://en.wikipedia.org/wiki/System_F)
* 2006: [Calculus of Constructions](https://en.wikipedia.org/wiki/Calculus_of_constructions)
* Type theory
* Modern advances, induction-recursion, pattern matching, implicits
* Modern type-theoretic proof assistants

 1952: Robin Gandy's thesis under Turing, which uses typed lambda-calculus for theory representation, and does (I think) the first logical relations proof to prove that the interpretation of lambda-terms is invariant under permutation.
  https://www.repository.cam.ac.uk/items/e0c1251f-07ad-4137-9305-3c94609a960e

- 1967: Nicolas De Bruijn's Automath system, which invented dependent types  https://en.wikipedia.org/wiki/Automath

- 1969: J. Roger Hindley: "The principal type-scheme of an object in combinatory logic". The Hindley half of Hindley-Milner.

- 1969: Lambek's "Deductive systems and categories II. Standard constructions and closed categories", which doesn't mention lambda-calculus explicitly, but is the first connection between closed categories and deduction systems like typed lambda-calculus.

- 1969: James Morris' thesis, which includes a bit on using typed lambda-calculus for programming https://dspace.mit.edu/handle/1721.1/64850

### How to read this book

Please resist the temptation to skip things. I know many books take everything extremely slowly,
but this is not one of them. We will go quite fast and everything is in here for a reason. There are
many "aha!" moments you will miss if you skip things.

### Apologetics

Idris is a cutting edge research project. Error messages are often inscrutable. It is a work in
progress, especially in user-friendliness.

# Unlearning what you know

Do you have experience of programming an imperative language? Forget most of what you know, we do
not play by the same set of rules. I will try to teach you as we go along but do not judge the code
I show you by the standards of the languages you already know because things work differently here.

* Inheritance
* Casting
* Pointers
* Memory layout

# Basic idris

Idris is really all about the definition of data structures, so let's get to know some of the ones
that are really useful!

Warning: don't skip over these, we build up the complexity quite quickly and use the opportunity for
some digressions to put things in context! They're also just really useful data structures that will
come up again and again in the real world.

We are going to introduce syntax as we go along, but a brief summary (which you may skip if you
trust us to explain as we go along or you know haskell in which case it'll look pretty familiar):

* Indentation is significant. The rules are quite simple, we will introduce them as we go.
* case of names is significant
  * in a pattern match, lowercase names will be assumed to be variables by default whereas
    uppercase names will be assumed to name things that exist thus:
    * modules, types and data constructors should have names beginning with a capital letter (e.g. Foo, Bar)
    * variables should have names beginning with a lowercase letter (e.g. foo, bar)
    * functions may have names beginning with either a capital or lowercase letter:
      * lowercase is preferred for most functions
      * If the return type of a function is `Type`, consider a capital.
      * If you are likely to use the function in a pattern match without calling it, consider a capital
* Function application is just space, parens are only needed for grouping:
  * `f x` - call `f` with argument `x`
  * `f x y` - call `f` with arguments `x`, `y`
  * xpect - `f (x y)` - call `f` with the result of calling `x` with `y`.
* The user may define infix operator functions.
* If functions and operators are mixed without parentheses, function calling is always highest
  precedence (happens first).
* `:` is used to indicate the type of something. You can pronounce it 'has type' if you like.
* `::` adds an item to the front of a list-shaped thing. You can pronounce it 'cons'.

If any of that didn't make sense, *don't worry*, we are going to explain it as we go along.

## The booleans

Most programming languages have a boolean type, but usually the `true` and `false` constants are
hardcoded into the parser. In idris, booleans are just a normal datatype:

```idris
data Bool : Type where
  True  : Bool
  False : Bool
```

Let's walk through that painfully slowly...

```idris
data Bool : Type where
```

We are declaring a datatype `Bool` of type `Type`. The indented body is going to list (all!) the
ways of constructing data of type `Bool`.

```idris
True : Bool
```

`True` is a `Bool`. That is you can use `True` anywhere you need to provide a `Bool`.

```idris
  False : Bool
```

`False` is a `Bool`. That is you can use `False` anywhere you need to provide a `Bool`.

Thus, we declare three names:

* `Bool`  of type `Type`
* `True`  of type `Bool`
* `False` of type `Bool`

The definition is closed, in the sense that you cannot add new ways of creating `Bool` data later -
it will forever be just `True` and `False` unless you edit the original definition.

We call `Bool` a `type constructor` because it constructs a `Type` whereas `True` and `False` are
known as `data constructors` as they construct data of a particular type (in this case, `Bool`).

### Tangent : meaning

Meaning is a human construct. The names `Bool`, `True` and `False` come from convention amongst
mathematicians and programmers, but they could equally be called "Cow", "Pig" and "Sheep" and as
long as their definition was the same, it wouldn't matter from a logical point of view.

We could even make `True` be the false value and `False` be the true value and so long as we are
*consistent* in our interpretation of them, it wouldn't matter. That said, you shouldn't do that
because you'll confuse everyone (including yourself).

### Simple functions

Sticking with the conventions for booleans for now, let's write a function which ANDs two booleans:

```idris
and : Bool -> Bool -> Bool
and True  True  = True
and True  False = False
and False True  = False
and False False = False
```

We first give a type to the function and then match the individual cases.

```idris
and : Bool -> Bool -> Bool
```

`and` is a function taking a `Bool` and a `Bool` and returning a `Bool`. The `->` represents
functions. The return of the function is of the rightmost type (`Bool`) and the preceding types are
the parameters (`Bool`, `Bool`). The reasons for this arrangement will become clear later.

```idris
and True  True  = True
```

When given `True` and `True`, the answer is `True`.

```idris
and True  False = False
```

When given `True` and `False`, the answer is `False`

```idris
and False True  = False
```

When given `False` and `True`, the answer is `False`

```idris
and False False = False
```

When given `False` and `False`, the answer is `False`.


We can define ORing two booleans similarly by pattern matching (though we won't step through it):

```idris
or : Bool -> Bool -> Bool
or True  True  = True
or True  False = True
or False True  = True
or False False = False
```

### Writing less code with pattern matching

In the previous section, we matched all possible combinations of two `Bool`s once each for `and` and
`or`, but this would be impractical if there were even 4 `Bools` (16 inputs). We can use variables
to simply name the inputs rather than matching against them too, for shorter definitions:

```idris
and : Bool -> Bool -> Bool
and True  True  = True
and x     y    = False

or : Bool -> Bool -> Bool
or False False = False
or x     y     = True
```

This works because matching considers patterns in the order they are written, with the first pattern
that matches being the one whose answer matters. Notice that variable names start with a lowercase
letter where type  started with an uppercase letter - this is significant!

We have defined these functions, but we haven't spoken about calling them. Function call syntax
looks much like function definition syntax, using spaces to separate the function name and any
arguments. We thus might literally write `and True True`. And let's look at how that would match:

```idris
and True  True  = True
```

Are the inputs `True` and `True`? Yes, so the answer is `True` and we are done.

That wasn't very exciting, let's look at `and True False` next:

```idris
and True  False  = False
```

Are the inputs `True` and `True`? No, go to the next pattern.

```idris
and x     y    = False
```

How about variables `x` and `y`? A variable can match anything, so yes. `x` is now `True` and `y` is
now False. We don't care about the values of them though - the answer is False, we are done.

If you aren't satisfied that our two versions of `and` and `or` are equivalent, feel free to step
through all the cases to convince yourself.

## Counting with the natural numbers

Let's go up a step in sophistication and look at the natural numbers (the non-negative integers). In
idris we can define them like this:

```idris
data Nat : Type where
  Z : Nat
  S : Nat -> Nat
```

Let's break that down:

```idris
data Nat : Type where
```

We are declaring a datatype called `Nat` of type `Type`. Our declaration's indented body will list
all of the ways to create a `Nat`.

```idris
Z : Nat
```

Z is of type `Nat`. Z stands for Zero.

```idris
S : Nat -> Nat
```

It's that `->` again, saying we need a `Nat` to make a `Nat`. Or put another way, `S` is a function from `Nat`
to `Nat`. S stands for `successor`. The successor of a `Nat` is another `Nat` (it represents adding 1).

So the naturals are either zero or the successor of another natural number. We can create any positive number
we like by applying `S` repeatedly to `Z`.

This representation is another convention and we usually call naturals defined this way "Peano numbers"
after Giuseppe Peano who formalised them. It has analogies with ancient times, where numbers were written as a
line for every 1 (though they hadn't invented zero back then!).

### Tangent : induction

We say that `Nat` is an `inductive` datatype because its definition refers to itself, specifically
that `S : Nat -> Nat`. You might be tempted to say it's a "recursive" definition, and in a sense it
is, but when we are doing recursion like this in a datastructure definition, we will call it
`induction`.

Mathematics has a concept of 'proof by induction', where an argument is split into a base case (like
`Z`) and a recursive case (like `S`). We can see `Nat` as encoding a demonstration of how you can
construct a number structurally. So a `Nat` is proof that this is how you can construct a number.

In a way, for every `S` we wrap a `Nat` with, we are proving that a larger number exists by
demonstrating how to create it. 

### Tangent : structural equality

In type theory, we have the concept of things being the same when they have the same structure
("structural equality"). For now we'll say "when they're written the same" and I'll explain more
later. Let's look at a few (we write structural equalities with `=` like in algebra):

```idris
True  = True       -- true, same structure
False = False      -- true, same structure
Z = Z             -- true, same structure
S Z = S Z         -- true, same structure
S (S Z) = S (S Z) -- true, same structure
True  = False      -- not true, True and False are different `Bool`s.
False = True       -- also not true for the same reason
S Z = Z           -- not true, `S anything is a different `Nat` from `Z`
Z = S Z           -- also not true for the same reason
True = Z          -- not true, not even the same type, `True` is a `Bool` and `Z` is a `Nat`!
```

Okay, now let's add variables and do some schoolyard algebra. Note that idris is a pure
functional language, so variables are not mutable. `x` and `y` are variables of type `Nat`

```idris
S x = Z   -- not true, S anything can never be equal to Z
S x = S x -- true, because x = x and S = S
S Z = S x -- true only if x = Z
S x = S y -- true only if x = y
```

### Arithmetic on Nats

```idris
plus : Nat -> Nat -> Nat
plus Z y = y
plus (S x) y = plus x (S y)
```

As usual, we'll go a line at a time...

```idris
plus : Nat -> Nat -> Nat
```

`plus` is a function taking a `Nat` and a `Nat` and returning a `Nat`

```idris
plus Z y = y
```

When the first `Nat` given to `plus` is `Z`, we return the other `Nat`. We say this is our base case
because we do not do any recursion (calling ourselves).

```idris
plus (S x) y = plus x (S y)
```

When the first `Nat` given to `plus` is the successor of another `Nat`, `x`, we call `plus` with
that `x` and the successor of the other `Nat`, `y`. Or to look at it another way, we call plus, but
with one taken one off of the first argument and added to the second. We call this the recursive
case because it calls itself to continue computing.

Because there are only two cases (`Z` and `S`), we have matched all possible `Nat`s. And we did it
through recursion (or if you *must* think of it imperatively, looping). We could say, then, that a
 `Nat` encodes repetition into its structure. Interesting...

### Tangent: recursion

In idris, we typically encode logic recursively. We saw induction earlier when defining datatypes,
but now we've seen recursion in action in `plus` (`plus (S x) y` calls `plus`).

If you are not familiar with pure functional programming, we don't use looping mechanisms like
`while` or `for`, we use recursion. Recursive definitions (much like inductive definitions) settle
into a standard form:

* A base case, the last step of the computation.
* A recursive case, which may be used arbitrarily many times until we reach the base case.

Much as you should not write a `for` loop where the termination condition is not met, you should not
write a recursive case which does not contribute to solving the problem somehow. Here is our
recursive case from the last section:

```idris
plus (S x) y = plus x (S y)
```

The contribution of this code may not be obvious. The pattern match `S x` on the first argument
means that `x` is one smaller than it would be if we had simply written `plus x y`. Thus when we
call `plus`, we are slightly closer to the base case than we started.

This pattern of 'unwrapping a level' in the recursive case is going to come up a *lot*, so let's get
comfortable with it. Let's recall the definition of `plus` for convenience:

```idris
plus : Nat -> Nat -> Nat
plus Z y = y
plus (S x) y = plus x (S y)
```

And now let's step through how `3 + 3` is calculated:

```idris
plus (S (S (S Z))) (S (S (S Z))) = plus (S (S Z)) (S (S (S (S Z)))) -- 3 + 3 = 2 + 4
plus (S (S Z)) (S (S (S (S Z)))) = plus (S Z) (S (S (S (S (S Z))))) -- 2 + 4 = 1 + 5
plus (S Z) (S (S (S (S (S Z))))) = plus Z (S (S (S (S (S (S Z)))))) -- 1 + 5 = 0 + 6
plus Z (S (S (S (S (S (S Z)))))) = S (S (S (S (S (S Z)))))          -- 0 + 6 = 6
```

You should be able to see we can give it any positive value and the first argument is always getting
smaller, so it will always eventually reach the base case.

Notice that where inductive type definitions *build up* a data structure, recursion allows us to
*tear it down* again. 

## Lists

One of the most useful data structures in functional programming is the list (of the singly-linked
variety). Where a nat merely encodes repetition, you could say a list encodes repeated storage.

```idris
data List : Type -> Type where
  Nil  : List t
  (::) : t -> List t -> List t
```

Again, we'll take it a line at a time.

```idris
data List : Type -> Type where
```

We are declaring a datatype `List` of type `Type -> Type`. You may recall that `->` is used for
functions, and indeed it behaves similarly here. `List` in itself is not a `Type`, we have to pass
it a `Type` to get a `Type`, so `List : Type -> Type` but `List Nat : Type`. You can see this as a
type-level function and we say that `List` is *parameterized* by a `Type`.

```idris
Nil : List t
```

`Nil` is a `List t`. It represents the empty list. Recall from pattern matching before that names
beginning with a lowercase letter are treated as variables - the same applies here

Since we know that `List : Type -> Type` and `Nil : List t`, we can infer that `t` must be a
`Type`. Thus `Nil` can create a `List t` for any type `t` we wish.


```idris
(::) : t -> List t -> List t
```

`::` (pronounced "cons") is a function taking a `t` and a `List t` and returning a `List t`. Put
another way, We can put a `t` on the front of an existing `List t` and get another `List t`. The `t`
matches the same `Type` throughout the signature, so we can only cons a `t` onto the a `List t`.

The parentheses are just idris notation for declaring operator functions (those made of
punctuation). I won't say too much about how operators work yet because it isn't germane to my
point, but we will use cons like `a :: b` in practice. I could equally call it `Cons`, but this is
the naming idris already uses, so we shall too.

### Functions on lists

```idris
sum : List Nat -> Nat
sum Nil = Z
sum (x :: xs) = plus x (sum xs)
```

```idris
sum : List Nat -> Nat
```

`sum` is a function taking a `List Nat` and returning a `Nat`

```idris
sum Nil = Z
```

When given the empty list, we return Z.

```idris
sum (x :: xs) = plus x (sum xs)
```

When given a `Nat`, `x` consed onto a `List Nat`, `xs`, we add `x` to the `sum` of `xs`.

Notice that like with `Nat`, we are peeling off one layer at a time in the recursive case, just this
time it's cons instead of `S`. I said we'd do a lot of that!

What if we want to multiply a list of numbers together? Firstly, we'll have to define
multiplication, which is just repeated addition:

```idris
mul : Nat -> Nat -> Nat
mul Z y = Z
mul (S x) y = plus y (mul x y)
```

Now we're ready to define the product of a list of numbers:

```idris
product : List Nat -> Nat
product Nil = (S Z)
product (x :: xs) = mul x (product xs)
```

Now let's bring back the definition of `sum` so you can compare them side by side:

```idris
sum : List Nat -> Nat
sum Nil = Z
sum (x :: xs) = plus x (sum xs)
```

Notice how the only things that differ are:
* The name of the function
* The return value in the base case
* The function called at each step

### Higher order functions on lists

Let's take advantage of the similarity we've just documented and make a more generic
alternative. This time we will take the function to apply each step and the default value as extra
parameters:

```idris
foldNat: (Nat -> Nat -> Nat) -> Nat -> List Nat -> Nat
foldNat f x Nil = x
foldNat f x (y :: ys) = f y (foldNat f x ys)
```

Let's go line by line as usual:

```idris
foldNat: (Nat -> Nat -> Nat) -> Nat -> List Nat -> Nat
```

`foldNat` is a function returning a `Nat` and taking three arguments:
* a `Nat -> Nat -> Nat` - a function taking two `Nat`s and returning a `Nat`
* a `Nat` - the 'default' value we will return in the base case.
* a `List Nat` - the list of numbers to operate on.

We will bind the first argument to `f` and the second argument to `x` and only match against the third.

```idris
foldNat f x Nil = x
```

When the third argument is `Nil`, the answer is the default value `x`.

```idris
foldNat f x (y :: ys) = f y (foldNat f x ys)
```

When the third argument is `y :: ys`, the answer is calling `f` with two arguments:

* `y` - the head of the list we matched.
* `foldNat f x ys` - the result of folding the rest of the list

We could go even more generic, `foldNat` still only works on a `List Nat`, whereas we can make any
`List t` where `t` has type `Type`.

```idris
fold: (x -> y -> y) -> y -> List x -> y
fold f x Nil = x
fold f x (y :: ys) = f y (fold f x ys)
```

The only part that differs significantly is the type signature:

```idris
fold: (x -> y -> y) -> y -> List x -> y
```

Here we are making much more use of variables, with `x` and `y` standing for any two types. Remember
that variables do not change their meaning across a type signature, so we are dealing with only two
types. `x` is the item type, the type of item contained in the `List x`. `y` is the return type, the
type the whole expression is expected to return.

We can write `foldNat` in terms of fold:

```idris
foldNat: (Nat -> Nat -> Nat) -> Nat -> List Nat -> Nat
foldNat = fold
```

Notice that this fixes both `x` and `y` to `Nat`. But the more general signature of `fold` allows us
more flexibility because `x` and `y` may be different types. A simple example is an identity
function (a function that returns its argument unchanged) for lists:

```idris
listIdentity : List x -> List x
listIdentity xs = fold (::) Nil xs
```

Intuitively, for every cons (`::`) we peel apart, we put it back together, thus the overall effect
is to do nothing. If we recall the types of cons and fold, you'll see how it fits:

```idris
(::) : t -> List t -> List t
fold: (x -> y -> y) -> y -> List x -> y
```

## Vectors

Vectors are like lists, except of a known length.

```idris
data Vect : Nat -> Type -> Type where
  Nil : Vect Z t
  (::) : t -> Vect n t -> Vect (S n) t
```

This one's a bit more complicated...

```idris
data Vect : Nat -> Type -> Type where
```

We are declaring a datatype called `Vect` (vector) which takes a `Nat` and a `Type` to form a `Type`. The
`Nat` represents the length of the vector (how many items are in it) and the `Type` represents the type of the
items in it.

```idris
Nil : Vect Z t
```

`Nil` has type `Vect Z t`, that is a vector containing zero entries of any type `t`. It's a lot like `Nil`
from `List`, but we fix the length to be zero.

```idris
(::) : t -> Vect n t -> Vect (S n) t
```

Our old friend 'cons' is back too. This time we cons a `t` onto a `Vect n t` to produce a `Vect (S n) t`. We
know that `Vect : Nat -> Type -> Type`, therefore `n` is a `Nat` and `t` is a `Type`. Notice that the vector
we are constructing is `S n` long, i.e. one longer than the vector we are consing onto.

Let's compare `::` from `List` with `::` from `Vect`:

```idris
(::) : t -> List   t -> List       t -- from List
(::) : t -> Vect n t -> Vect (S n) t -- from Vect
```

Very similar indeed except the `Nat` is changing in the `Vect` version. Recall that we said `List` is
*parameterized by* a `Type`. So, too is `Vect`, but what about this `Nat`? Would we say that `Vect` is
*parameterized by* a `Nat`?

What's going on here is a little more involved. The `Type` parameter never varies at all in either `List` or
`Vect`, but that `Nat` changes with each element we add. We say that `Vect` is *indexed by* a `Nat`. In the
case of a `Vect` this makes intuitive sense, the `Nat` is counting from the end of the `Vector`, so it
corresponds to indexing in a list, it's just we've moved it into the type.

But what does it mean for `Vect` to be indexed by a `Nat`? Well what if you know what that `Nat` is? If it's
`Z`, you know it can only be constructed by `Nil`. If it's `S n`, you know it can only be constructed by
`::`. In other words the index can determine which constructors could have created the data.

### Familiar functions on vectors

Let's welcome back our old friend `sum`, but this time adapted for lists:

```idris
sum : Vect n Nat -> Nat
sum Nil = Z
sum (x :: xs) = plus x (sum xs)
```
 
We've seen everything here before except the type signature has changed. Note how we just bind the length of
the vector to a variable `n` and gain the ability to work on vectors of any length.

Here's another old friend adapted, and again, all we've changed is the type signature:

```idris
foldNat: (Nat -> Nat -> Nat) -> Nat -> Vect n Nat -> Nat
foldNat f x Nil = x
foldNat f x (y :: ys) = f y (foldNat f x ys)
```

How about the generic fold?

```idris
fold: (x -> y -> y) -> y -> Vect n x -> y
fold f x Nil = x
fold f x (y :: ys) = f y (fold f x ys)
```

Huh, it's pretty neat that all we had to do is slightly update the types for all three of these.

### Differently familiar functions on vectors

```idris
map: (x -> y) -> Vect n x -> Vect n y
map f Nil = Nil
map f (y :: ys) = f y :: map f ys
```

Chances are you recognise `map` from elsewhere. We won't walk through it step by step, but note that the
vector passed in and the vector returned are the same length. Unlike if we wrote it for lists, our `map` on
vectors is required to conserve the length!

# Practical idris

The aim of this section is to teach you enough idris programming that you can start to play with
it. Programming is meant to be fun!

## Monads and IO

There are a vast number of articles trying to explain what monads are (burritos! programmable semicolons!
monoids in the category of endofunctors!). Instead, we shall learn to use monads and not worry too much about
what they mean.

In the idris we have covered so far, everything is nice pure deterministic functions - that is to say that
given the same input, you will always get the same output. But how can this hold for example if you read a
line of keyboard input twice when the user may enter two different lines? Clearly we need some different
rules. Let's start by looking at the types involved so we can get a feel for what we're working with:

```idris
interface Monad (m : Type -> Type) where
  pure : a -> m a
  (>>=) : m a -> (a -> m b) -> m b
```

Ooh, an interface! We haven't seen those yet!

```idris
interface Monad (m : Type -> Type) where
```

We are declaring an interface called `Monad` over some `m : Type -> Type`. Our main focus for `m` will be `IO`
but `Maybe` and `List` are also valid.

```idris
pure : a -> m a
```

`pure` turns any `a` into an `m a`. E.g. for `a=Nat` and `m=IO`, `pure : Nat -> IO Nat`. Essentially, it
allows you to `lift` a pure value into the monad (to convert the type by wrapping it in `m`).

```idris
(>>=) : m a -> (a -> m b) -> m b
```

`>>=` ("bind") takes an `m a` and a function from `a -> m b` and returns `m b`.

This one's a little weird. It takes a `a` which is already wrapped in `m` and a function of `a` (now
unwrapped) to `m b` (wrapped again!). What gives?

Firstly, note the asymmetry of `Monad` - `pure` gives us an easy way to lift pure data *into* `m`, but it
doesn't provide us an easy way to get it back *out* again. Actually, the type of `bind` is designed to give us
access to the values wrapped in `m` while ensuring that we don't leave it!

If you're familiar with 'continuation passing style' (CPS), you should recognise the function we are passing
is a continuation.

Well, firstly, note there is no inverse of `pure` in `Monad`, no function from `m a -> a`. You can lift values
into the monad, but there doesn't seem to be a way of getting them back out again.

## Techniques

* Data.List.Quantifiers.All
  * `All (const t) list` - A list-like structure of `t` the same length as `list`. Cheap pairing!
  * `All p list` - A list of `p x` for each `x` in `list` - cheap per-element dependency!

## Dependency

Dependency is amazing, but we should not overuse it. One example of overuse is `All (const t) list`
that i recommended in the previous section. Here, even though we only wish to store a list of `t`
the same length as `list`, we have promoted our correlation to a dependency which is explicitly not
needed since we are using `const`. This is fine for simple things, but in more complex scenarios
means we now have to deal with that dependency and introduce more data structures to deal with it all.

Sometimes we just want a coincidence or correlation (for each item, another item exists, but it is
not dependent on the first item). The correct tool for this is a pair of `Vect` and the index of the
item in the first vector is usable to get the corresponding item in the second vector.

## Understanding errors



## Glossary

Boolean
: Datatype representing one bit of information by two alternatives, conventionally "True" and "False"

Constructor
:
Data Constructor
:
Family
: 
Fold
:
Identity function
: A function which returns its argument unchanged
Indexed type
:
Induction
: 
Inductive datatype
:
Inductive family
:
Natural numbers
:
List
:

Peano numbers
:
Proof-irrelevant
: Terms that are axiomatically equal when the types match
Proof-relevant
: Terms that are equal when their normal form is the same
Set
:
Type Constructor
:

Vector
:

## Notation

`t[s/x]` - `(\x => t) s` (t, with x = s therein)
`Pi x: A, B[a/x]` - `(x : A) -> B` (called with some unbound `a : A` to substitute for x)

This syntax is a bit weird, but if you go with as a function, it says you can pick an `a : A` to
replace `x` with in the definition of `B` (which thus may use `x` in its definition) by calling it.



