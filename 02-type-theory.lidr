# Type theory

## Lambda calculus


### Reduction

#### Alpha conversion (renaming)



Our motivation is the following basic truth: `x * 2 = 2 * x`. We're going to apply it to functions to formalise when expressions are 

#### Beta reduction (application)



alpha conversion, beta reduction

An express for which a reduction procedure would change the value is a reducible expression (redex)
or specifically alpha-redex, beta-redex. the result of reduction is called the `reduct`
(alpha-reduct, beta-reduct).

#### Eta reduction

wikipedia:

 > η-reduction (eta reduction) expresses the idea of extensionality,[25] which in this context is that two functions are the same if and only if they give the same result for all arguments. η-reduction converts between λx.f x and f whenever x does not appear free in f.

