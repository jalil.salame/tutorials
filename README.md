# Tutorials

## Idris

1. [Setting up idris](00-setup.md)
2. [Idris introduction](01-introduction.md)

[idris library docs](https://www.idris-lang.org/docs/idris2/current/)


## Other stuff

The type theory tutorial is incredibly barebones and the category theory stuff sort of went wrong and you should probably ignore them for now.

