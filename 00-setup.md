# Setup to learn idris
    
## Installing idris

Warning: are you using Mac OS on apple silicon? There are some problems with this setup, ask for details.

The general approach is:

1. Install needed software from your distribution's package manager:
  * alpine: `doas apk install build-base make chez-scheme gmp-dev`
  * debian/ubuntu: `sudo apt install build-essential chezscheme libgmp-dev`
2. Figure out what your distribution calls the chez scheme *binary*:
  * alpine: `chez`
  * debian/ubuntu: `chezscheme`
  * other: try running `hash -r` and tab-completing `chez` and see what comes back
3. Install pack (you will need that binary name from the last step):
   `bash -c "$(curl -fsSL https://raw.githubusercontent.com/stefan-hoeck/idris2-pack/main/install.bash)"`
   Note: this is single threaded and may take a while (10 minutes on a 4.3ghz ryzen 9 3900X, 90 minutes on a 2014 potato)
4. Add `$HOME/.pack/bin` to your PATH environment variable: `export PATH=$HOME/.pack/bin:$PATH`
  * Run it in your current shell to find it now.
  * Add it to your shell's rc file (`~/.bashrc`, `~/.zshrc` etc.) to make it work in all newly spawned shells.

You should now be able to run `pack repl` and get an idris repl.

We recommend enabling readline support by setting `repl.rlwrap` to `true` in `~/.pack/user/pack.toml`

### Nix/NixOS

If you use [`nix`](https://nixos.org) then you can use the following instructions to install `pack`.

```console
$ nix shell nixpkgs#gnumake nixpkgs#chez nixpkgs#gmp.dev nixpkgs#git nixpkgs#curl nixpkgs#gcc nixpkgs#bash nixpkgs#coreutils 
$ bash -c "$(curl -fsSL https://raw.githubusercontent.com/stefan-hoeck/idris2-pack/main/install.bash)"
```

> Remember to add `chez` to your profile, it will be used to compile idris code afterwards.
>
> You can do this with `nix profile install nixpkgs#chez`

## Editor integrations

Emacs: Install [idris-mode](https://github.com/idris-hackers/idris-mode) from melpa.
Vim: Install [idris2-vim](https://github.com/edwinb/idris2-vim)
Neovim: [see here](https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#idris2_lsp)
VSCode: [idris2-lsp-vscode](https://github.com/bamboo/idris2-lsp-vscode)

## Setting up an idris project

You should create an `ipkg` file in the project root. For a project called `myproject`, you'd call
it `myproject.ipkg` and you can start with this template:

```ipkg
package myproject

depends = base, contrib

sourcedir = "src"
```

You can now create files in the `src` directory. Don't be tempted to set `sourcedir` to `.` because
at least the emacs tooling gets a bit confused if you do.

## Installing libraries

You shouldn't need any to start playing with idris, but if you do, `pack` has your back.

E.g. to install the `profunctors` library: `pack install profunctors`

For a list of packages:

* determine which package collection you are using with `pack info`
* find its configuration [here](https://github.com/stefan-hoeck/idris2-pack-db/tree/main/collections)

## Updating idris and libraries

1. Run `pack update-db`
2. Run `ls ~/.pack/db | sort | tail -n 1` and note the output (mine says `nightly-231224.toml`)
3. Edit `~/.pack/user/pack.toml`. Change the `collection` config to match what you found in step 2
   (without `.toml`). for the example: `collection = "nightly-231224"`
4. Run `pack update` (this may take a while...)
